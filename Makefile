# Author(s): Thibaut ALLAIN
# Contact: thibaut.allain@orange.com

##################################################
################ DEFINE VARIABLES ################
##################################################
STACK_NAME:= "tac-demo"
EXTERNAL_SERVICE:= "front-tier"
COMPOSE_FILE:= "docker-compose.yml"
##################################################


# Export environnment variables
# export LEADER := $(shell docker node ls | grep Leader -c)
# export NETWORK := $(shell docker network ls | grep -e monitor -e portainer_agent -e traefik -c)

# Source : https://stackoverflow.com/a/14061796/2142790
# Pass a variable without the argument name
SUPPORTED_COMMANDS := get_service update_service get_logs inspect_service rm_service
SUPPORTS_MAKE_ARGS := $(findstring $(firstword $(MAKECMDGOALS)), $(SUPPORTED_COMMANDS))
ifneq "$(SUPPORTS_MAKE_ARGS)" ""
  COMMAND_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(COMMAND_ARGS):;@:)
endif

# List of tasks
help:
	@echo ''
	@echo 'Usage: make [TASK] [EXTRA_ARGUMENTS]'
	@echo 'Tasks:'
	@echo '  init    			Initializing the swarm manager.'
	@echo '  initNetwork    	Create external traefik network.'
	@echo '  rmNetwork    		Remove external traefik network.'
	@echo '  deploy_stack  		Deploy the stack.'
	@echo '  rm_stack     		Remove a stack. Be carefull ! All running tasks will be deleted.'
	@echo '  list_stack   		List tasks.'
	@echo '  list_tasks   		List all tasks of the stack.'
	@echo '  get_service    	Get specific service.'
	@echo '  inspect_service  	Inspect specified service. '
	@echo '  update_service     Update running service.'
	@echo '  rm_service    	Remove a service.'
	@echo '  list_service    	List all services.'
	@echo '  get_logs      		Show logs for target service.'
	@echo '  clean      		Clean all data.'
	@echo ''
	@echo 'Extra arguments:'
	@echo 'target:				make get_service prometheus'

init: ## Initialize swarm manager
ifeq ($(LEADER), 1) 
	$(info Make: Initialization abord. Already done.)
else 
	$(info Make: Initializing swarm manager.)
	@docker swarm init 
endif

initNetwork: ## Initialize network
ifeq ($(NETWORK), 3) 
	$(info Make: Create networks abord. Already done.)
else 
	$(info Make: Creating networks.)
	@docker network create -d overlay $(EXTERNAL_SERVICE)
endif

rmNetwork: ## Remove network
ifeq ($(NETWORK), 0) 
	$(info Make: Deletion networks abord. None exists.)
else 
	$(info Make: Removing networks.)
	@docker network rm $(EXTERNAL_SERVICE)
endif

deploy_stack: ## Deploy the stack
	$(info Make: Starting all containers.)
	@env `cat .env | grep ^[A-Z] | xargs` docker stack deploy --compose-file $(COMPOSE_FILE) $(STACK_NAME)

rm_stack: ## Destroy the stack 
	$(info Make: Destroying all containers.)
	@docker stack rm $(STACK_NAME)

list_stack: ## List stack
	$(info Make: Listing stack.)
	@docker stack ls --orchestrator=swarm

list_tasks: ## List tasks in the stack
	$(info Make: List the tasks in the stack)
	@docker stack ps $(STACK_NAME)

get_service: ## List the service
	$(info Make: Display the  specified service)
	@docker service ps --no-trunc $(STACK_NAME)_$(COMMAND_ARGS) 

rm_service: ## Remove a service
	$(info Make: Remove the  specified service)
	@docker service rm $(STACK_NAME)_$(COMMAND_ARGS) 


inspect_service: ## Inspect the service
	$(info Make: Inspect the  specified service)
	@docker service  inspect $(STACK_NAME)_$(COMMAND_ARGS) 

list_service: ## List all services in the cluster	
	$(info Make: List all the service)
	@docker service ls

update_service: ## List a service in the cluster	
	$(info Make: List all the service)
	@docker service update $(STACK_NAME)_$(COMMAND_ARGS) 

get_logs: ## get the logs of a service
	$(info Make: Get the logs of a service)
	@docker service logs $(STACK_NAME)_$(COMMAND_ARGS) 

clean: ## Clean all data
	$(info Make: cleaning system.)
	@docker system prune --volumes --force