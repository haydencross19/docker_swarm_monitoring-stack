# SetUp the swarm cluster

### 0/ Install Docker and docker-compose


The commands for this could be found below :
https://docs.docker.com/compose/install/

### 1/ Initialize the swarm cluster
````
cloud@bastion-zen:$ docker swarm init
Swarm initialized: current node (c678t1g183gmtau3lgfg7cx6j) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-28evtfok8kyjqgvgnt7mwqfwa4ja8b6irv20sarajzudvpycu8-463m7wkhgu5kne1chlbdkejui 172.16.1.52:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
````

### 2/ Add the nodes to the cluster
````
cloud@docker-nodes-0001:~$ docker swarm join --token SWMTKN-1-28evtfok8kyjqgvgnt7mwqfwa4ja8b6irv20sarajzudvpycu8-463m7wkhgu5kne1chlbdkejui 172.16.1.52:2377
This node joined a swarm as a worker.
````

### 3/ List the nodes within the cluster
````
cloud@bastion-zen:$ docker node ls
ID                            HOSTNAME            STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
s3gqo1klpoul9hwhubyi13hcx *   bastion-zen         Ready     Active         Leader           20.10.3
dj67qza2f754m0p7yofwnud8r     docker-nodes-0001   Ready     Active                          20.10.3
jhpr18rhgx1sxjgkpg84hj5n7     docker-nodes-0002   Ready     Active                          20.10.3
````

### 4/ Destroy the Cluster
````
cloud@bastion-zen:$ docker swarm leave --force
Node left the swarm.
````