## Presentation 
Deploy a full Grafana/Prometheus stack within a Docker swarm Cluster. 

Are embedded the tools for collecting data, operating the solution and the networking part thanks to Traefik. 

## Default Packages 

The version of the packages could be updated in the .env file. 


| Tools   | Docker images   |      Last Version      |  Description |
|----------|:----------|:-------------:|:------|
| **Traefik** | traefik |  2.2.2 | Networking - Route incoming requests |
| **Prometheus** | bitnami/prometheus |  1.17.8 | Monitoring server |
| **Grafana** | bitnami/grafana |    7.4.1   |   Dashboards and Performance |
| **Alertmanager** | bitnami/alertmanager | 0.21.0 |    Route alerts to 3rd party |
| **Karma** | lmierzwa/karma | latest |    A light supervision tool |
| **Portainer** | portainer/portainer-ce |2.16.0 |    Docker Management |
| **Portainer-agent** | portainer/agent | 1.5.1 |    Portainer agent for swarm cluster |
| **Node-Exporter** | prom/node-exporter | latest |    Nodes metrics |
| **cAdvisor** | google/cadvisor | v0.33.0 |    Containers metrics |
| **Blackbox Exporter** | bitnami/blackbox-exporter | 0.18.0 |    Web testing |

## README Files

These files will explain how to set up the prerequisites, configure the cluster and deploy the applications. 

| Filenames   | Description   |
|----------|:----------|
|0_persistency.md| Install convoy and configure the persistent volumes.|
|1_setupSwarmCluster.md| SetUp the Swarm Cluster and deploy the Nodes.|
|2_deployTheStack.md| Deploy the applications.|
