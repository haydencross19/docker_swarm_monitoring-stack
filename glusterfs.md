
# Install glusterfs package
````
$ sudo apt install -y glusterfs-server glusterfs-client;
$ sudo systemctl start glusterd && sudo systemctl enable glusterd
````

# Complete the hosts file
````
$ cat /etc/hosts
127.0.0.1 localhost.localdomain localhost
172.16.1.52 bastion-zen
172.16.1.209 docker-nodes-001
172.16.1.136 docker-nodes-002
````

# Probing the nodes
````
$ gluster peer probe docker-nodes-001; gluster peer probe docker-nodes-002;
peer probe: success.
peer probe: success.
````
# List the server within the cluster 
````
$ gluster pool list
UUID                                    Hostname                State
24d88716-090b-4fa5-8249-4a819979f037    docker-nodes-001        Connected
f9306df6-c3c4-415c-bac0-037b89da762f    docker-nodes-002        Connected
edffdd3a-d8bf-4b15-ba0c-6c5dd3615bce    localhost               Connected
````

# FROM ALL NODES
# Create an empty volume
````
$ sudo mkdir -p /gluster/tac
````

# FROM MASTER
# Create the volume accross the cluster ON THE MASTER
````
$ sudo gluster volume create staging-gfs replica 3 bastion-zen:/gluster/tac docker-nodes-001:/gluster/tac docker-nodes-002:/gluster/tac force
volume create: staging-gfs: success: please start the volume to access data
````

# Start the volume ON THE MASTER
````
$ sudo gluster volume start staging-gfs
volume start: staging-gfs: succes
````

# Allow mount connection only from localhost
````
$ sudo gluster volume set staging-gfs auth.allow 127.0.0.1
volume set: success
````

# FROM ALL NODES
# Mount the volume on a reboot
````
$ echo 'localhost:/staging-gfs /mnt glusterfs defaults,_netdev, backupvolfile-server=localhost 0 0' >> /etc/fstab
$ mount.glusterfs localhost:/staging-gfs /mn
$ chown -R root:docker /mnt
````