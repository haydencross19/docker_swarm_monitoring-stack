 
# Persistency

**Convoy** makes it easy to manage your data in Docker. It provides persistent volumes for Docker containers with support for snapshots, backups, and restores on various back-ends (e.g. device mapper, NFS, EBS).

## Setup NFS Server 

#### Install NFS ON MASTER NODE
````
$ sudo apt-get install nfs-kernel-server nfs-common -y
$ mkdir /data
$ chown -R nobody:nogroup /data
$ echo '/data 172.16.1.52(rw,no_root_squash,sync,no_subtree_check) 172.16.1.209(rw,no_root_squash,sync,no_subtree_check) 172.16.1.136(rw,no_root_squash,sync,no_subtree_check)' >> /etc/exports
$ sudo systemctl restart nfs-kernel-server
$ sudo systemctl enable nfs-kernel-server
````

#### Install NFS Clients ON ALL NODES (Master + Nodes)

````
$ sudo apt-get install nfs-common -y
$ mount 172.16.1.52:/data /mnt
$ df -h
# Mount the volume at the boot
$ sudo bash -c "echo '172.16.1.52:/data /mnt nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0' >> /etc/fstab"
````
## Setup Convoy 

#### Install Rancher Convoy Plugin ON ALL NODES (Master + Nodes)
````
$ wget https://github.com/rancher/convoy/releases/download/v0.5.0/convoy.tar.gz
$ tar xzf convoy.tar.gz
$ sudo cp convoy/convoy convoy/convoy-pdata_tools /usr/local/bin/
$ sudo mkdir -p /etc/docker/plugins/
$ sudo bash -c 'echo "unix:///var/run/convoy/convoy.sock" > /etc/docker/plugins/convoy.spec'
````

#### The source should be cloned in a repository accessible from all nodes 

````
# FROM MASTER NODE
# Get the code from gitlab
$ git clone https://gitlab.com/haydencross19/tac_monitoring-stack.git
````

#### Create file /etc/init.d/convoy 
````
#!/bin/sh
### BEGIN INIT INFO
# Provides:
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start daemon at boot time
# Description:       Enable service provided by daemon.
### END INIT INFO

dir="/usr/local/bin"
cmd="convoy daemon --drivers vfs --driver-opts vfs.path=/mnt/docker/volumes"
user="root"
name="convoy"

pid_file="/var/run/$name.pid"
stdout_log="/var/log/$name.log"
stderr_log="/var/log/$name.err"

get_pid() {
    cat "$pid_file"
}

is_running() {
    [ -f "$pid_file" ] && ps `get_pid` > /dev/null 2>&1
}

case "$1" in
    start)
    if is_running; then
        echo "Already started"
    else
        echo "Starting $name"
        cd "$dir"
        if [ -z "$user" ]; then
            sudo $cmd >> "$stdout_log" 2>> "$stderr_log" &
        else
            sudo -u "$user" $cmd >> "$stdout_log" 2>> "$stderr_log" &
        fi
        echo $! > "$pid_file"
        if ! is_running; then
            echo "Unable to start, see $stdout_log and $stderr_log"
            exit 1
        fi
    fi
    ;;
    stop)
    if is_running; then
        echo -n "Stopping $name.."
        kill `get_pid`
        for i in {1..10}
        do
            if ! is_running; then
                break
            fi

            echo -n "."
            sleep 1
        done
        echo

        if is_running; then
            echo "Not stopped; may still be shutting down or shutdown may have failed"
            exit 1
        else
            echo "Stopped"
            if [ -f "$pid_file" ]; then
                rm "$pid_file"
            fi
        fi
    else
        echo "Not running"
    fi
    ;;
    restart)
    $0 stop
    if is_running; then
        echo "Unable to stop, will not attempt to start"
        exit 1
    fi
    $0 start
    ;;
    status)
    if is_running; then
        echo "Running"
    else
        echo "Stopped"
        exit 1
    fi
    ;;
    *)
    echo "Usage: $0 {start|stop|restart|status}"
    exit 1
    ;;
esac

exit 0
````

#### Start Convoy 
````
$ chmod +x /etc/init.d/convoy
$ sudo systemctl enable convoy
$ sudo /etc/init.d/convoy start
````

#### Test Convoy 
````
$ convoy create test1
test1

$ docker volume ls
DRIVER              VOLUME NAME
convoy              test1
````

[Source](https://blog.ruanbekker.com/blog/2018/02/16/guide-to-setup-docker-convoy-volume-driver-for-docker-swarm-with-nfs/)