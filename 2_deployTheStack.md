# Deploy the applications

### Prerequisites

The Make package is mandatory for using the Makefile (often already installed on linux server).

````
$ apt install make
````

### Basic Commands

Deploy the stack

````
$ env $(cat .env | grep ^[A-Z] | xargs) docker stack deploy --compose-file docker-compose.yml tac-demo
Updating service tac-demo_cadvisor (id: n84bkjhzfo3p1447oogsccsta)
Updating service tac-demo_grafana (id: zlh3dtt658senjiljsdrklyfr)
Updating service tac-demo_traefik (id: p3341ednpzjvqnab36ou14gcl)
Updating service tac-demo_karma (id: fuvf2i9bu3d5bdsg4k11xnuuu)
Updating service tac-demo_blackbox_exporter (id: dqamrzgw3yap03w0q1sacsdcc)
Updating service tac-demo_prometheus (id: 4b61r8p49cy8lby2jv1iejfmu)
Updating service tac-demo_node-exporter (id: 2smjnk4yqquzz4mvz3phqaptu)
Updating service tac-demo_portainer-agent (id: ta735hibl55l7l0r5gykpp1vn)
Updating service tac-demo_alertmanager (id: rt425norh9h652rqdxe3i1qqm)
Updating service tac-demo_portainer (id: 65hbfc1315qno200d4duwa5vq)
````

Display the services within the task
````
docker stack ps tac-demo
ID             NAME                                                 IMAGE                              NODE                DESIRED STATE   CURRENT STATE                 ERROR                         PORTS
9odty4mv75ha   tac-demo_alertmanager.1                              bitnami/alertmanager:0.21.0        docker-nodes-0001   Running         Running about a minute ago
vz7yt0893ky2    \_ tac-demo_alertmanager.1                          bitnami/alertmanager:0.21.0        docker-nodes-0001   Shutdown        Shutdown about a minute ago
vum9kji5tvvj    \_ tac-demo_alertmanager.1                          bitnami/alertmanager:0.21.0        docker-nodes-0001   Shutdown        Shutdown 31 hours ago
wqs1csol970m   tac-demo_cadvisor.dj67qza2f754m0p7yofwnud8r          google/cadvisor:latest             docker-nodes-0001   Running         Running 4 days ago
km4qhm4vbr78   tac-demo_cadvisor.jhpr18rhgx1sxjgkpg84hj5n7          google/cadvisor:latest             docker-nodes-0002   Running         Running 4 days ago
wrfy45ypfhoh   tac-demo_cadvisor.s3gqo1klpoul9hwhubyi13hcx          google/cadvisor:latest             bastion-zen         Running         Running 4 days ago
````

### Use Makefile
````
$ make help

Usage: make [TASK] [EXTRA_ARGUMENTS]
Tasks:
  init                          Initializing the swarm manager.
  initNetwork                   Create external traefik network.
  rmNetwork                     Remove external traefik network.
  deploy_stack                  Deploy the stack.
  rm_stack                      Remove a stack. Be carefull ! All running tasks will be deleted.
  list_stack                    List tasks.
  list_all_tasks                List all tasks of the stack.
  get_service                   Get specified service.
  list_services                 List all services.
  inspect_service              Inspect specified service. 
  update_service                Update running service.
  get_logs                      Show logs for target service.
  clean                         Clean all data.

Extra arguments:
target:                         make up monitoring_prometheus
````
An example : 
````

$ make get_service prometheus
Make: Display the  specified service
ID                          NAME                        IMAGE                                                                                               NODE                DESIRED STATE   CURRENT STATE           ERROR                       PORTS
0mck0h3u23cy8kwcr0n79t1d4   tac-demo_prometheus.1       bitnami/prometheus:2.24.1@sha256:230447f4a9c08af88c87ca7ac7291c41782c10d5eb7b5b0e97ec2fc25e623b59   docker-nodes-0002   Running         Running 22 hours ago
vfdpb4dufaaz98jecn5u4sfi9    \_ tac-demo_prometheus.1   bitnami/prometheus:2.24.1@sha256:230447f4a9c08af88c87ca7ac7291c41782c10d5eb7b5b0e97ec2fc25e623b59   docker-nodes-0002   Shutdown        Shutdown 22 hours ago
ry93hkn9eeeyix9rfy2himoiw    \_ tac-demo_prometheus.1   bitnami/prometheus:2.24.1@sha256:230447f4a9c08af88c87ca7ac7291c41782c10d5eb7b5b0e97ec2fc25e623b59   docker-nodes-0002   Shutdown        Failed 22 hours ago     "task: non-zero exit (1)"
r8xn64fvna1q0r2mx1ffaaekk    \_ tac-demo_prometheus.1   bitnami/prometheus:2.24.1@sha256:230447f4a9c08af88c87ca7ac7291c41782c10d5eb7b5b0e97ec2fc25e623b59   docker-nodes-0002   Shutdown        Failed 22 hours ago     "task: non-zero exit (1)"
0t3v7m9eh87yjkfmpwfl302mu    \_ tac-demo_prometheus.1   bitnami/prometheus:2.24.1@sha256:230447f4a9c08af88c87ca7ac7291c41782c10d5eb7b5b0e97ec2fc25e623b59   docker-nodes-0002   Shutdown        Failed 22 hours ago     "task: non-zero exit (1)"
make: 'prometheus' is up to date.
````